from abc import ABC,abstractmethod

#--------------------------------------------------------- base----------------------------------------------------------------

# class StatDisplay():
#     def __init__(self):
#         pass
    
#     def _update(self,temp : float ,humid : float, pression : float):
#         print(temp,humid,pression)
        
    
# class CurrentDisplay():
#     def __init__(self):
#         pass
    
#     def _update(self,temp : float ,humid : float, pression : float):
#         print(temp,humid,pression)
    
    
# class WeatherData():
    
#     def __init__(self,stat : StatDisplay, current : CurrentDisplay):
#          self._stat_display = stat
#          self._current_display = current

#     def _getTemperature(self):
#         return 6.3

#     def _getHumidity(self):
#         return 3.9
    
#     def _getPressure(self):
#         return 3.7
    
#     def _measurmentChanged(self):
#         self._current_display._update(self._getTemperature(), self._getHumidity(), self._getPressure())
#         self._stat_display._update(self._getTemperature(), self._getHumidity(), self._getPressure())
#         print("changed")
        
        
     
        
# if __name__ == '__main__':
    
#     wither = WeatherData(StatDisplay(), CurrentDisplay())
#     wither._measurmentChanged()
    
    
    
#----------------------------------------------new--------------------------------------------------------------------------------


class Display (ABC):

    @abstractmethod
    def _update(self,data : {}):
        pass
        
        
class Current (Display):
    def __init__(self):
        self.__data = {}
    
    def _update(self,data):
        print(data)

class Stat(Display):
    def __init__(self):
        self.__data = {}
    
    def _update(self,data):
        print(data)



class WeatherData():
    
    def __init__(self,stat : Stat, current : Current):
         self._displays = []

    def _getData(self):
        return {'temp':5.3,'humid':5.9,'press':3.5}
    
    def _add(self,display :Display):
        self._displays.append(display)
    
    def _measurmentChanged(self):
        for display in self._displays:
            display._update(self._getData())
        print("changed")


if __name__ == '__main__':
    
    wither = WeatherData(Stat(), Current())
    wither._add(Stat())
    wither._add(Current())
    wither._measurmentChanged()
input()
