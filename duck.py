from abc import ABC,abstractmethod


 class Flyable(ABC):
     def __init__(self):
         pass
     @abstractmethod
     def fly(self):
         pass


class Fly1(Flyable):
     def __init__(self):
         pass
     def fly(self):
         print("je vole")

class Fly2(Flyable):
     def __init__(self):
        
         pass
    
     def fly(self):
         print("je vole aussi")


class Duck(ABC):
    
     def __init__(self,flyable :Flyable):
         self._flying_methods = flyable
    

     def fly(self):
         self._flying_methods.fly()
         #print("fly")
    
    
     def quack(self):
         print("quack")
    
    
     def setFlyable(self,flyable :Flyable):
         self._flying_methods = flyable.fly()
    
     def swim(self):
        print("swim")
    
     @abstractmethod 
     def display(self):
         pass



 class Mallard_Duck(Duck):
    
     def __init__(self,flayable :Flyable):
          super().__init__(flayable)
    
        
     def __quack(self):
         print("specific quacking method")

     def display(self):
         print("Mallard_Duck")


 class RedHeadDuck(Duck):
    
     def __init__(self,flayable :Flyable):
          super().__init__(flayable)
         

        
     def __quack(self):
         print("specific quacking method")

     def display(self):
         print("Mallard_Duck")



 if __name__ == '__main__':
     redDeadRedempduck = RedHeadDuck(Fly1())
     redDeadRedempduck.fly()
     redDeadRedempduck.setFlyable(Fly2())
