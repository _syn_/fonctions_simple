# -*- coding: utf-8 -*-
"""
Created on Wed Dec  7 08:38:19 2022
@author: syn
spyder : 4.2.5
python : 3.8.8
"""

import re

def solution(sequence = ''):
    new_sequence = sequence
    REPEATER = re.compile(r"(.+?)\1+$")
    match = REPEATER.match(new_sequence)
    if match : return new_sequence.count(match.group(1))
    else: return 1
    
def solution2(sequence = ' '):
    if len(sequence) > 1:
        pattern_init = ""
        for i in range(len(sequence)):
            pattern_init += sequence[i]
            if sequence.rsplit(pattern_init).count('') == len(sequence.rsplit(pattern_init)):
                return sequence.count(pattern_init)
    return 1
